How to make it run
============

The project is build using maven. 


#Build And Test

  * mvn package
  
The command has to be launched within this directory. 

#Run

  * mvn spring-boot:run
  
  The command has to be launched within this directory.
  
# Configuration

Inside the folder *resources* there is an application.properties file with these key:

 * logging.level.org.springframework.web=DEBUG
 * spring.profiles.active=springdatajpa
 * remote.api.daily=http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml
 * remote.api.90Days=http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml
 * days.before=90

Each configuration are self-explanatory

#Usage

When the application is started, it is possible to query the api rest via these commands:

 * curl localhost:8080/getrates

This command will retrieve the last 90 days rates for every currencies that have been retrieved 
from the remote api.

 * curl localhost:8080/getrates/CHF
 
This command will retrieve the last 90 days rates for the specified currency
 
 * curl localhost:8080/getrates/CHF/2017/05/15
 
This command will retrieve the rate for the specified currency and for the specified date.
The url format follow this convention: [CURRENCY]/[YEAR]/[MONTH]/[DAY]

The answer is always in a JSON format.
The answer is always a JSON array.
 
If for the specified date there aren't any rates the result is a empty array.
If some pieces of the REST url request are missing the result is an error.


#Suggestions
It is suggested to pipe the result of the curl with the program **jq**.

Under OSX it is installable via brew with this command:

 * brew install jq
 
 
 
 #examples
 
 * Request of a particular currency rate in a particular date:
 ~~~
 $ curl localhost:8080/getrates/CHF/2017/05/12 |jq
   % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                  Dload  Upload   Total   Spent    Left  Speed
 100    66    0    66    0     0   4977      0 --:--:-- --:--:-- --:--:--  5076
 [
   {
     "currency": "CHF",
     "rates": [
       {
         "date": "2017-05-12",
         "rate": 1.0963
       }
     ]
   }
 ]
 ~~~
 
 * Request of all the rates for a particular currency on the last 90 days
 ~~~
  $ curl localhost:8080/getrates/CHF | jq
   % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                  Dload  Upload   Total   Spent    Left  Speed
 100  2253    0  2253    0     0   159k      0 --:--:-- --:--:-- --:--:--  169k
 [
   {
     "currency": "CHF",
     "rates": [
       {
         "date": "2017-05-12",
         "rate": 1.0963
       },
       {
         "date": "2017-05-11",
         "rate": 1.0952
       },
       {
         "date": "2017-05-10",
         "rate": 1.0949
       },
 *
 * Omissis
 *
       {
         "date": "2017-02-13",
         "rate": 1.0673
       }
     ]
   }
 ]
 ~~~
 
 * Request all rates for all currencies
 
 ~~~
 $ curl localhost:8080/getrates/CHF | jq -C . | more -r
   % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                  Dload  Upload   Total   Spent    Left  Speed
 100  2253    0  2253    0     0   236k      0 --:--:-- --:--:-- --:--:--  244k
 
 [
   {
     "currency": "CHF",
     "rates": [
       {
         "date": "2017-05-12",
         "rate": 1.0963
       },
       {
         "date": "2017-05-11",
         "rate": 1.0952
       },
       *
       * Omissis
       *
      {
        "date": "2017-02-13",
        "rate": 1.0673
       }
     ]
   },
   {
     "currency": "HRK",
     "rates": [
       {
         "date": "2017-05-12",
         "rate": 7.4225
       },
       
       *
       * omissis
       *
      ]
    }
    *
    * omissis 
  }
 ] 
       
 ~~~
 
 * Not existing currency request
 
 ~~~
  $ curl localhost:8080/getrates/TESTS | jq -C
   % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                  Dload  Upload   Total   Spent    Left  Speed
 100     2    0     2    0     0    222      0 --:--:-- --:--:-- --:--:--   250
 []
 ~~~
 
 * Not existing date for currency request
  
  ~~~
   $ curl localhost:8080/getrates/CHF/2017/05/14 | jq -C .
    % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                   Dload  Upload   Total   Spent    Left  Speed
  100     2    0     2    0     0    220      0 --:--:-- --:--:-- --:--:--   222
  []
  ~~~
  
  * Bad requests
  
  ~~~
   $ curl localhost:8080/getrates/CHF/BADREQUEST |jq -C . | more -r
    % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                   Dload  Upload   Total   Spent    Left  Speed
  100   127    0   127    0     0  13381      0 --:--:-- --:--:-- --:--:-- 14111
  {
    "timestamp": 1494800773014,
    "status": 404,
    "error": "Not Found",
    "message": "No message available",
    "path": "/getrates/CHF/BADREQUEST"
  }
  ~~~
  
  ~~~

 $ curl localhost:8080/getrates/CHF/BAD/REQUEST/DONE |jq -C
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   360    0   360    0     0  31361      0 --:--:-- --:--:-- --:--:-- 32727
{
  "timestamp": 1494800800226,
  "status": 400,
  "error": "Bad Request",
  "exception": "org.springframework.web.method.annotation.MethodArgumentTypeMismatchException",
  "message": "Failed to convert value of type 'java.lang.String' to required type 'int'; nested exception is java.lang.NumberFormatException: For input string: \"BAD\"",
  "path": "/getrates/CHF/BAD/REQUEST/DONE"
}
  ~~~
  
  ~~~
   $ curl localhost:8080/getrates/CHF/2017 | jq -C 
    % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                   Dload  Upload   Total   Spent    Left  Speed
  100   121    0   121    0     0  11882      0 --:--:-- --:--:-- --:--:-- 12100
  {
    "timestamp": 1494800842038,
    "status": 404,
    "error": "Not Found",
    "message": "No message available",
    "path": "/getrates/CHF/2017"
  }
  ~~~