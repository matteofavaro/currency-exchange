package com.app.sample.exchange.task;

import com.app.sample.exchange.entity.CurrencyDateRate;
import com.app.sample.exchange.service.CurrencyService;
import com.app.sample.exchange.service.RemoteECPGateway;
import com.app.sample.exchange.service.XmlParserService;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by matteofavaro on 06/05/2017.
 */
@Component
public class JobECB {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  @Autowired
  XmlParserService xps;

  @Autowired
  CurrencyService currencyService;

  @Autowired
  RemoteECPGateway gtw;

  private LocalDateTime nextExecution = LocalDateTime.now().minusMinutes(1);

  /**
   * examples of other CRON expressions
   * "0 0 * * * *" = the top of every hour of every day.
   * "* /10 * * * * *" = every ten seconds.
   * "0 0 8-10 * * *" = 8, 9 and 10 o'clock of every day.
   * "0 0/30 8-10 * * *" = 8:00, 8:30, 9:00, 9:30 and 10 o'clock every day.
   * "0 0 9-17 * * MON-FRI" = on the hour nine-to-five weekdays
   * "0 0 0 25 12  = every Christmas Day at midnight
   * fixedRate = milliseconds
   */

  @Scheduled(fixedRate = 60000) // every 60 seconds 60000
  public void retrieveRemoteExchangeRates() {

    try {
      log.debug("scheduled running...");
      if (LocalDateTime.now().isAfter(nextExecution)) {

        List<CurrencyDateRate> latestCurs = gtw.retriveLastestCurrencies();
        Optional<LocalDate> latest = currencyService.getLatest();

        //Filterning out the db
        if (latest.isPresent()) {
          latestCurs = latestCurs.stream()
              .filter(s -> s.getDate().isAfter(latest.get()))
              .collect(
                  Collectors.toList()
              );
        }
        // persisting into db
        currencyService.saveCurrency(latestCurs);

        nextExecution = LocalDateTime.now().plusDays(1);
        log.info("next execution time:" + nextExecution.toString());
      }

    } catch (Exception e) {
      log.error(e.getMessage());
      nextExecution = LocalDateTime.now().plusMinutes(1);
      log.info("Rescheduling task in 1 minute");
    }
    log.info("job Done. Next Execution: " + nextExecution.toString());
  }


}
