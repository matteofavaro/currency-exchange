package com.app.sample.exchange.service;

import com.app.sample.exchange.entity.CurrencyDateRate;
import java.util.List;

/**
 * Created by matteofavaro on 11/05/2017.
 */
public interface RemoteECPGateway {
  List<CurrencyDateRate> retriveLastestCurrencies();
}
