package com.app.sample.exchange.service;

import com.app.sample.exchange.util.xml.CubeWithTime;
import com.app.sample.exchange.util.xml.Envelope;
import com.google.common.collect.Lists;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


/**
 * Created by matteofavaro on 05/05/2017.
 */
@Service
public class XmlParserService {

  @Value("${remote.api.daily}")
  private String remoteApiDaily;

  @Value("${remote.api.90Days}")
  private String remoteApi90Days;

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  private RestTemplate restTemplate;

  public XmlParserService() {
    this.restTemplate = new RestTemplate();
  }
  public XmlParserService(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  public Optional<CubeWithTime> getDailyECBRates() {
    Optional<CubeWithTime> ret = Optional.empty();
    try {
      log.debug(remoteApiDaily);
      ret = Optional.of(getObjects(remoteApiDaily).getCube().getCubesWithTime().get(0));
    } catch (Exception e) {
      log.error(e.toString());
      log.error(e.getMessage());
    }
    return ret;

  }

  public List<CubeWithTime> get90DaysECBRates() {

    List<CubeWithTime> ret = Lists.newArrayList();
    try {
      log.debug(remoteApi90Days);
      ret.addAll(getObjects(remoteApi90Days).getCube().getCubesWithTime());
    } catch (Exception e) {
      log.error(e.toString());
      log.error(e.getMessage());
    }

    return ret;
  }

  private Envelope getObjects(String remoteApi) {
    return restTemplate.getForObject(remoteApi, Envelope.class);
  }



}
