package com.app.sample.exchange.service;

import com.app.sample.exchange.entity.CurrencyDateRate;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Created by matteofavaro on 09/05/2017.
 */
public interface CurrencyService {

  List<CurrencyDateRate> getLastDays(int days);
  List<CurrencyDateRate> getRatesByCurrency(int days, String currency);
  List<CurrencyDateRate> getRatesByCurrencyAndDate(String currency, LocalDate date);

  void saveCurrency(List<CurrencyDateRate> currencyDateRate);

  Optional<LocalDate> getLatest();

}
