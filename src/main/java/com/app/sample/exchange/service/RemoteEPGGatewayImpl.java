package com.app.sample.exchange.service;

import com.app.sample.exchange.entity.CurrencyDateRate;
import com.app.sample.exchange.util.xml.CubeWithTime;
import com.google.common.collect.Lists;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by matteofavaro on 11/05/2017.
 */
@Service
public class RemoteEPGGatewayImpl implements RemoteECPGateway {


  private XmlParserService xps;

  @Autowired
  public RemoteEPGGatewayImpl(XmlParserService xps) {
    this.xps = xps;
  }

  @Override
  public List<CurrencyDateRate> retriveLastestCurrencies() {

    List<CurrencyDateRate> ret = Lists.newArrayList();

    Optional<CubeWithTime> daily = xps.getDailyECBRates();
    List<CubeWithTime> last90Days = xps.get90DaysECBRates();

    if (daily.isPresent()) {
      LocalDate t = daily.get().getDate();
      if (last90Days.stream().filter(cwt -> cwt.getDate().equals(t)).count() == 0) {
        last90Days.add(daily.get());
      }

    }

    last90Days.stream().forEach(dayList -> {
      dayList.getCubesWithRateAndCurrency().stream().forEach(cubeCurrency -> {
            ret.add(CurrencyDateRate
                .builder()
                .date(dayList.getDate())
                .rate(cubeCurrency.getRate())
                .currency(cubeCurrency.getCurrency())
                .build());
          }
      );

    });

    return ret;
  }
}
