package com.app.sample.exchange.service;

import com.app.sample.exchange.entity.CurrencyDateRate;
import com.app.sample.exchange.repository.CurrencyDateRateRepository;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by matteofavaro on 09/05/2017.
 */
@Service
public class CurrencyServiceImpl implements CurrencyService {

  private CurrencyDateRateRepository currencyDateRateRepository;


  @Autowired
  public CurrencyServiceImpl(
      CurrencyDateRateRepository currencyDateRateRepository) {
    this.currencyDateRateRepository = currencyDateRateRepository;
  }

  @Override
  public List<CurrencyDateRate> getLastDays(int days) {
    return currencyDateRateRepository.getRates(days);
  }

  @Override
  public List<CurrencyDateRate> getRatesByCurrency(int days, String currency) {
    return currencyDateRateRepository.getRatesByCurrency(days, currency);
  }

  @Override
  public List<CurrencyDateRate> getRatesByCurrencyAndDate(String currency, LocalDate date) {
    return currencyDateRateRepository.getRatesByCurrencyAndDate(currency, date.toString());
  }

  @Override
  public void saveCurrency(List<CurrencyDateRate> currencyDateRate) {
    currencyDateRateRepository.save(currencyDateRate);
  }

  @Override
  public Optional<LocalDate> getLatest() {
    return Optional.ofNullable(currencyDateRateRepository.getLatest());
  }
}
