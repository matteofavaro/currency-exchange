package com.app.sample.exchange.entity;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by matteofavaro on 06/05/2017.
 */
@Entity
public class CurrencyDateRate {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer id;

  @Column
  private LocalDate date;
  @Column
  private String currency;
  @Column
  private Double rate;


  public CurrencyDateRate() {}

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public LocalDate getDate() {
    return date;
  }

  public String getCurrency() {
    return currency;
  }

  public Double getRate() {
    return rate;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public void setRate(Double rate) {
    this.rate = rate;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof CurrencyDateRate) {
      CurrencyDateRate t = (CurrencyDateRate) obj;
      return t.getDate().equals(this.getDate()) &&
          t.getCurrency().equals(this.getCurrency()) &&
          t.getRate().equals(this.getRate());
    }
    return false;
  }

  public CurrencyDateRate(Builder builder) {
    this.rate = builder.rate;
    this.currency = builder.currency;
    this.date = builder.date;
  }


  public static Builder builder() {
    return new Builder();
  }


  public static class Builder {

    private LocalDate date;
    private String currency;
    private Double rate;


    public CurrencyDateRate.Builder currency(String currency) {
      this.currency = currency;
      return this;
    }

    public CurrencyDateRate.Builder rate(Double rate) {
      this.rate = rate;
      return this;
    }

    public CurrencyDateRate.Builder date(LocalDate date) {
      this.date = date;
      return this;
    }

    public CurrencyDateRate build() {
      return new CurrencyDateRate(this);
    }

  }


}
