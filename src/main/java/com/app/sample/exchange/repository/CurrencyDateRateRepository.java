package com.app.sample.exchange.repository;

import com.app.sample.exchange.entity.CurrencyDateRate;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by matteofavaro on 06/05/2017.
 */
public interface CurrencyDateRateRepository extends
    JpaRepository<CurrencyDateRate, Long> {//CrudRepository<CurrencyDateRate, Long> {

  @Query(value = "SELECT * FROM CURRENCY_DATE_RATE where DATEDIFF('DAY', CURRENT_DATE, date) < ?1", nativeQuery = true)
  List<CurrencyDateRate> getRates(@Param("days") int days);


  @Query(value = "SELECT MAX(DATE) FROM CURRENCY_DATE_RATE", nativeQuery = true)
  LocalDate getLatest();

  @Query(value = "SELECT * FROM CURRENCY_DATE_RATE where DATEDIFF('DAY', CURRENT_DATE, date) < ?1 AND CURRENCY = ?2", nativeQuery = true)
  List<CurrencyDateRate> getRatesByCurrency(
      @Param("days") int days,
      @Param("currency") String currency);

  @Query(value = "SELECT * FROM CURRENCY_DATE_RATE where CURRENCY = ?1 AND date= ?2", nativeQuery = true)
  List<CurrencyDateRate> getRatesByCurrencyAndDate(
      @Param("currency") String currency, @Param("date") String date
  );


}
