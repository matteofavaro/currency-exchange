package com.app.sample.exchange.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Created by matteofavaro on 07/05/2017.
 */
@Configuration
@EnableScheduling
public class ScheduledTaskExecutor {
}
