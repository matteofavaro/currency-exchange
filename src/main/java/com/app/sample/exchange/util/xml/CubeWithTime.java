package com.app.sample.exchange.util.xml;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * Created by matteofavaro on 08/05/2017.
 */
public class CubeWithTime implements Comparable<CubeWithTime> {


  @JacksonXmlProperty(localName = "Cube")
  @JacksonXmlElementWrapper(useWrapping = false)
  private List<CubeCurrency> cubesWithRateAndCurrency;

  @JacksonXmlProperty(localName = "time", isAttribute = true)
  private String time;

  public CubeWithTime() {}

  public String getTime() {
    return time;
  }

  public void setTime(String time) {
    this.time = time;
  }

  public LocalDate getDate() {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    return LocalDate.parse(time, formatter);
  }

  public List<CubeCurrency> getCubesWithRateAndCurrency() {
    return cubesWithRateAndCurrency;
  }

  public void setCubesWithRateAndCurrency(List<CubeCurrency> cubesWithRateAndCurrency) {
    this.cubesWithRateAndCurrency = cubesWithRateAndCurrency;
  }

  @Override
  public String toString() {
    return getDate().toString();
  }

  @Override
  public int hashCode() {
    return getDate().toString().hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof CubeWithTime) {
      CubeWithTime t = (CubeWithTime) obj;
      return this.getDate().equals(t.getDate()) &&
          this.getCubesWithRateAndCurrency().equals(t.getCubesWithRateAndCurrency());
    }
    return false;
  }

  @Override
  public int compareTo(CubeWithTime o) {
    return o.getDate().compareTo(this.getDate());
  }

  public CubeWithTime(Builder builder) {
    setCubesWithRateAndCurrency(builder.cubesWithRateAndCurrency);
    setTime(builder.time);
  }

  public static Builder builder() {
    return new Builder();
  }

  public static class Builder {

    private List<CubeCurrency> cubesWithRateAndCurrency;
    private String time;

    public Builder cubesWithRateAndCurrency(List<CubeCurrency> cubesWithRateAndCurrency) {
      this.cubesWithRateAndCurrency = cubesWithRateAndCurrency;
      return this;
    }

    public Builder time(String time) {
      this.time = time;
      return this;
    }

    public CubeWithTime build() {
      return new CubeWithTime(this);
    }

  }

}
