package com.app.sample.exchange.util.json;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * Created by matteofavaro on 05/05/2017.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DailyRate {

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
  private String date;
  private Double rate;

  public String getDate() {
    return date;
  }

  public Double getRate() {
    return rate;
  }

  @Override
  public String toString() {
    return "DailyRate{" +
        "date=" + date +
        ", conversionRate=" + rate +
        '}';
  }

  public DailyRate(Builder builder) {
    this.date = builder.date;
    this.rate = builder.rate;
  }

  public static Builder builder() {
    return new Builder();
  }

  public static class Builder {

    private String date;
    private Double rate;


    public Builder date(String date) {
      this.date = date;
      return this;
    }

    public Builder rate(Double rate) {
      this.rate = rate;
      return this;
    }

    public DailyRate build() {
      return new DailyRate(this);
    }


  }

}


