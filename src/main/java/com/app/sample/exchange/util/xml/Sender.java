package com.app.sample.exchange.util.xml;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * Created by matteofavaro on 07/05/2017.
 */
public class Sender {

  @JacksonXmlProperty(localName = "name", namespace = "gesmes")
  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "sender name: " + name;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof Sender) {
      Sender t = (Sender) obj;
      return this.getName().equals(t.getName());
    }
    return false;
  }

  public Sender() {}

  public Sender(Builder builder) {
    setName(builder.name);
  }

  public static Builder builder() {
    return new Builder();
  }

  public static class Builder {

    private String name;

    public Builder name(String name) {
      this.name = name;
      return this;
    }

    public Sender build() {
      return new Sender(this);
    }
  }
}
