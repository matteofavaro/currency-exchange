package com.app.sample.exchange.util.xml;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import java.util.List;

/**
 * Created by matteofavaro on 08/05/2017.
 */
public class Cube {

  @JacksonXmlProperty(localName = "Cube")
  @JacksonXmlElementWrapper(useWrapping = false)
  private List<CubeWithTime> cubesWithTime;

  public List<CubeWithTime> getCubesWithTime() {
    return cubesWithTime;
  }

  public void setCubesWithTime(List<CubeWithTime> cubesWithTime) {
    this.cubesWithTime = cubesWithTime;
  }

  public Cube() {}

  public Cube(Builder builder) {
    this.cubesWithTime = builder.cubesWithTime;
  }

  public static Builder builder() {
    return new Builder();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof Cube) {
      return cubesWithTime.equals(((Cube) obj).getCubesWithTime());
    }
    return false;
  }

  public static class Builder {

    private List<CubeWithTime> cubesWithTime;


    public Builder cubesWithTime(List<CubeWithTime> cubesWithTime) {
      this.cubesWithTime = cubesWithTime;
      return this;
    }


    public Cube build() {
      return new Cube(this);
    }

  }


}
