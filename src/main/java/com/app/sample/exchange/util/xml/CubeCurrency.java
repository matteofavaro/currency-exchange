package com.app.sample.exchange.util.xml;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * Created by matteofavaro on 07/05/2017.
 */
public class CubeCurrency {


  @JacksonXmlProperty(localName = "currency", isAttribute = true)
  private String currency;

  @JacksonXmlProperty(localName = "rate", isAttribute = true)
  private Double rate;

  public CubeCurrency() {}

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public Double getRate() {
    return rate;
  }

  public void setRate(Double rate) {
    this.rate = rate;
  }

  @Override
  public String toString() {
    return getCurrency() + ": " + getRate();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof CubeCurrency) {
      CubeCurrency t = (CubeCurrency) obj;
      return this.getCurrency().equals(t.getCurrency()) && this.getRate().equals(t.getRate());
    }
    return false;
  }

  public CubeCurrency(Builder builder) {
    this.currency = builder.currency;
    this.rate = builder.rate;
  }

  public static Builder builder() {
    return new Builder();
  }

  public static class Builder {

    private String currency;
    private Double rate;

    public Builder currency(String currency) {
      this.currency = currency;
      return this;
    }

    public Builder rate(Double rate) {
      this.rate = rate;
      return this;
    }

    public CubeCurrency build() {
      return new CubeCurrency(this);
    }

  }


}
