package com.app.sample.exchange.util.converter;

import java.sql.Date;
import java.time.LocalDate;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by matteofavaro on 09/05/2017.
 */
@Converter(autoApply = true)
public class LocalDateTimeAttributeConverter implements AttributeConverter<LocalDate, Date> {

  @Override
  public Date convertToDatabaseColumn(LocalDate locDate) {
    return (locDate == null ? null : java.sql.Date.valueOf(locDate));
  }

  @Override
  public LocalDate convertToEntityAttribute(Date sqldate) {
    return (sqldate == null ? null : sqldate.toLocalDate());
  }
}