package com.app.sample.exchange.util.xml;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;


/**
 * Created by matteofavaro on 07/05/2017.
 */
@JacksonXmlRootElement(localName = "Envelope", namespace = "gesmes")
public class Envelope {

  @JacksonXmlProperty(localName = "subject", namespace = "gesmes")
  private String subject;

  @JacksonXmlProperty(localName = "Sender", namespace = "gesmes")
  private Sender sender;

  @JacksonXmlProperty(localName = "Cube", namespace = "")
  private Cube cube;

  public Envelope() {};

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public Sender getSender() {
    return sender;
  }

  public void setSender(Sender sender) {
    this.sender = sender;
  }

  public Cube getCube() {
    return cube;
  }

  public void setCube(Cube cube) {
    this.cube = cube;
  }

  @Override
  public String toString() {
    return "root: " + subject.toString();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof Envelope) {
      Envelope t = (Envelope) obj;
      return this.getSender().equals(t.getSender()) &&
          this.getSubject().equals(t.getSubject()) &&
          this.getCube().equals(t.getCube());
    }
    return false;
  }

  public Envelope(Builder builder) {
    setSubject(builder.subject);
    setSender(builder.sender);
    setCube(builder.cube);
  }

  public static Builder builder() {
    return new Builder();
  }

  public static class Builder {

    private String subject;
    private Sender sender;
    private Cube cube;

    public Builder subject(String subject) {
      this.subject = subject;
      return this;
    }

    public Builder sender(Sender sender) {
      this.sender = sender;
      return this;
    }

    public Builder cube(Cube cube) {
      this.cube = cube;
      return this;
    }

    public Envelope build() {
      return new Envelope(this);
    }

  }


}
