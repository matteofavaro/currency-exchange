package com.app.sample.exchange.util.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.collect.Lists;
import java.util.List;

/**
 * Created by matteofavaro on 05/05/2017.
 */

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CurrencyRates {

  private String currency;
  private List<DailyRate> rates;

  public String getCurrency() {
    return currency;
  }

  public List<DailyRate> getRates() {
    return rates;
  }

  public CurrencyRates(Builder builder) {
    this.rates = builder.rates;
    this.currency = builder.currency;
  }

  @Override
  public String toString() {
    return "CurrencyRates{" +
        "currency='" + currency + '\'' +
        ", rates=" + rates +
        '}';
  }

  public static Builder builder() {
    return new Builder();
  }


  public static class Builder {

    private String currency;
    private List<DailyRate> rates = Lists.newArrayList();


    public Builder currency(String currency) {
      this.currency = currency;
      return this;
    }

    public Builder dailyRates(List<DailyRate> rates) {
      this.rates = rates;
      return this;
    }

    public CurrencyRates build() {
      return new CurrencyRates(this);
    }

  }

}
