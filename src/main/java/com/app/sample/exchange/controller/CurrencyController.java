package com.app.sample.exchange.controller;

import com.app.sample.exchange.entity.CurrencyDateRate;
import com.app.sample.exchange.service.CurrencyService;
import com.app.sample.exchange.util.json.CurrencyRates;
import com.app.sample.exchange.util.json.DailyRate;
import com.google.common.collect.Lists;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by matteofavaro on 05/05/2017.
 */
@RestController
@RequestMapping("/getrates")
public class CurrencyController {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  @Value("${days.before}")
  private int numDays;

  @Autowired
  private CurrencyService currencyService;

  @RequestMapping("")
  @ResponseBody
  public List<CurrencyRates> getAllCurrencies() {
    return getCurrencyRates(Optional.empty(), Optional.empty());
  }

  @RequestMapping("/{currency}")
  @ResponseBody
  public List<CurrencyRates> getCurrency(@PathVariable String currency) {

    return getCurrencyRates(Optional.of(currency), Optional.empty());
  }

  @RequestMapping("/{currency}/{year}/{month}/{day}")
  @ResponseBody
  public List<CurrencyRates> getCurrencyDate(@PathVariable String currency,
      @PathVariable int year,
      @PathVariable int month,
      @PathVariable int day) {
    return getCurrencyRates(Optional.of(currency), Optional.of(LocalDate.of(year, month, day)));
  }

  public int getNumDays() {
    return numDays;
  }

  public void setNumDays(int numDays) {
    this.numDays = numDays;
  }

  private List<CurrencyRates> getCurrencyRates(Optional<String> currency,
      Optional<LocalDate> date) {

    currency.ifPresent(c -> log.info("Currency: {} ", c));
    date.ifPresent(d -> log.info("Date: {} ", d));
    List<CurrencyDateRate> stored;

    if (currency.isPresent()) {
      if (date.isPresent()) {
        stored = currencyService.getRatesByCurrencyAndDate(currency.get(), date.get());
      } else {
        stored = currencyService.getRatesByCurrency(numDays, currency.get());
      }
    } else {
      stored = currencyService.getLastDays(numDays);
    }

    Map<String, List<DailyRate>> result = new HashMap<>();
    //O(storedCurrencyRates.size())
    //access time to hash O(1)
    stored.forEach(scr -> {
      if (result.containsKey(scr.getCurrency())) {
        result.get(scr.getCurrency())
            .add(
                DailyRate.builder().rate(scr.getRate()).date(scr.getDate().toString()).build()
            );
      } else {
        result.put(scr.getCurrency(), Lists
            .newArrayList(
                DailyRate.builder().rate(scr.getRate()).date(scr.getDate().toString()).build()
            )
        );
      }
    });
    //O(number of currency)
    //access time to hash O(1)
    List<CurrencyRates> ret = new ArrayList<CurrencyRates>();
    result.keySet().forEach(
        k -> ret.add(
            CurrencyRates.builder().currency(k).dailyRates(result.get(k)).build()
        )
    );

    return ret;
  }

}
