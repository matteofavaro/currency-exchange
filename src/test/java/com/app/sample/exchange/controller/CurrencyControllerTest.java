package com.app.sample.exchange.controller;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.app.sample.exchange.entity.CurrencyDateRate;
import com.app.sample.exchange.service.CurrencyService;
import java.time.LocalDate;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 * Created by matteofavaro on 14/05/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class CurrencyControllerTest {

  @Mock
  CurrencyService currencyService;

  @InjectMocks
  CurrencyController controllerUnderTest;

  private MockMvc mockMvc;

  @Before
  public void setUp() {

    mockMvc = MockMvcBuilders.standaloneSetup(controllerUnderTest).build();
  }

  @Test
  public void testGetRatesNoParameter() throws Exception {

    LocalDate d = LocalDate.of(2017, 05, 14);

    controllerUnderTest.setNumDays(90);

    when(currencyService.getLastDays(90)).thenReturn(Lists.newArrayList(
        CurrencyDateRate.builder().currency("TEST0").rate(1.0).date(d).build(),
        CurrencyDateRate.builder().currency("TEST1").rate(1.0).date(d).build()
    ));

    MvcResult action = mockMvc.perform(
        MockMvcRequestBuilders.get("/getrates")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is2xxSuccessful())
        .andExpect(jsonPath("$", hasSize(2)))
        .andExpect(jsonPath("$[0].currency", is("TEST0")))
        .andExpect(jsonPath("$[0].rates", hasSize(1)))
        .andExpect(jsonPath("$[0].rates[0].date", is("2017-05-14")))
        .andExpect(jsonPath("$[0].rates[0].rate", is(1.0)))
        .andExpect(jsonPath("$[0].currency", is("TEST0")))
        .andExpect(jsonPath("$[1].rates", hasSize(1)))
        .andExpect(jsonPath("$[1].rates[0].date", is("2017-05-14")))
        .andExpect(jsonPath("$[1].rates[0].rate", is(1.0)))
        .andReturn();


  }

  @Test
  public void testGetRatesCurrencyParamenter() throws Exception {

    LocalDate d0 = LocalDate.of(2017, 05, 14);
    LocalDate d1 = LocalDate.of(2017, 05, 13);

    controllerUnderTest.setNumDays(90);

    when(currencyService.getRatesByCurrency(90, "TEST0")).thenReturn(Lists.newArrayList(
        CurrencyDateRate.builder().currency("TEST0").rate(1.0).date(d0).build(),
        CurrencyDateRate.builder().currency("TEST0").rate(2.0).date(d1).build()
    ));

    MvcResult action = mockMvc.perform(
        MockMvcRequestBuilders.get("/getrates/TEST0")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is2xxSuccessful())
        .andExpect(jsonPath("$", hasSize(1)))
        .andExpect(jsonPath("$[0].currency", is("TEST0")))
        .andExpect(jsonPath("$[0].rates", hasSize(2)))
        .andExpect(jsonPath("$[0].rates[0].date", is("2017-05-14")))
        .andExpect(jsonPath("$[0].rates[0].rate", is(1.0)))
        .andExpect(jsonPath("$[0].currency", is("TEST0")))
        .andExpect(jsonPath("$[0].rates[1].date", is("2017-05-13")))
        .andExpect(jsonPath("$[0].rates[1].rate", is(2.0)))
        .andReturn();
  }

  @Test
  public void testGetRatesCurrencyAndDateParamenter() throws Exception {

    LocalDate d0 = LocalDate.of(2017, 05, 14);

    controllerUnderTest.setNumDays(90);

    when(currencyService.getRatesByCurrencyAndDate("TEST0", d0)).thenReturn(Lists.newArrayList(
        CurrencyDateRate.builder().currency("TEST0").rate(1.0).date(d0).build()
    ));

    MvcResult action = mockMvc.perform(
        MockMvcRequestBuilders.get("/getrates/TEST0/2017/05/14")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is2xxSuccessful())
        .andExpect(jsonPath("$", hasSize(1)))
        .andExpect(jsonPath("$[0].currency", is("TEST0")))
        .andExpect(jsonPath("$[0].rates", hasSize(1)))
        .andExpect(jsonPath("$[0].rates[0].date", is("2017-05-14")))
        .andExpect(jsonPath("$[0].rates[0].rate", is(1.0)))
        .andReturn();
  }


}