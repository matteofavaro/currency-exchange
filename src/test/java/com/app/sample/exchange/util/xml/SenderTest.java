package test.com.app.sample.exchange.util.xml;

import static org.junit.Assert.*;

import com.app.sample.exchange.util.xml.Sender;
import org.junit.Test;

/**
 * Created by matteofavaro on 13/05/2017.
 */
public class SenderTest {

  @Test
  public void testEquals() throws Exception {
    Sender first = Sender.builder().name("sender").build();
    Sender second = Sender.builder().name("sender").build();
    assertTrue(first.equals(second));
  }

}