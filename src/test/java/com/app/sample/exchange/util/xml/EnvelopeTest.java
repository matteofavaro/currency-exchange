package test.com.app.sample.exchange.util.xml;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.app.sample.exchange.util.xml.Cube;
import com.app.sample.exchange.util.xml.CubeCurrency;
import com.app.sample.exchange.util.xml.CubeWithTime;
import com.app.sample.exchange.util.xml.Envelope;
import com.app.sample.exchange.util.xml.Sender;
import java.util.List;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by matteofavaro on 13/05/2017.
 */
public class EnvelopeTest {

  private List<CubeCurrency> correctFirstCurAndRate;
  private List<CubeCurrency> correctSecondCurAndRate;
  private List<CubeCurrency> errorSecondCurAndRate;

  private List<CubeWithTime> correctFirstCubeWithTime;
  private List<CubeWithTime> correctSecondCubeWithTime;
  private List<CubeWithTime> errorSecondDiffTimeSameRates;
  private List<CubeWithTime> errorSecondDiffRatesSameTime;

  private Sender correctSenderFirst;
  private Sender correctSenderSecond;
  private Sender errorSenderSecond;

  private Cube correctCubeFirst;
  private Cube correctCubeSecond;
  private Cube errorCubeSecondDiffTimeSameRates;
  private Cube errorCubeSecondDiffRatesSameTime;

  @Before
  public void setup() {

    correctFirstCurAndRate = Lists.newArrayList(
        CubeCurrency.builder().currency("USD").rate(1.0876).build(),
        CubeCurrency.builder().currency("JPY").rate(123.82).build(),
        CubeCurrency.builder().currency("BGN").rate(1.9558).build(),
        CubeCurrency.builder().currency("CHF").rate(1.0963).build()
    );
    correctSecondCurAndRate = Lists.newArrayList(
        CubeCurrency.builder().currency("USD").rate(1.0876).build(),
        CubeCurrency.builder().currency("JPY").rate(123.82).build(),
        CubeCurrency.builder().currency("BGN").rate(1.9558).build(),
        CubeCurrency.builder().currency("CHF").rate(1.0963).build()
    );
    errorSecondCurAndRate = Lists.newArrayList(
        CubeCurrency.builder().currency("USD").rate(1.0876).build(),
        CubeCurrency.builder().currency("JPY").rate(123.82).build(),
        CubeCurrency.builder().currency("BGN").rate(1.9558).build()
    );

    correctFirstCubeWithTime = Lists.newArrayList(
        CubeWithTime.builder().cubesWithRateAndCurrency(correctFirstCurAndRate).time("2017-05-12")
            .build()
    );
    correctSecondCubeWithTime = Lists.newArrayList(
        CubeWithTime.builder().cubesWithRateAndCurrency(correctSecondCurAndRate).time("2017-05-12")
            .build()
    );
    errorSecondDiffTimeSameRates = Lists.newArrayList(
        CubeWithTime.builder().cubesWithRateAndCurrency(correctSecondCurAndRate).time("2017-05-13")
            .build()
    );
    errorSecondDiffRatesSameTime = Lists.newArrayList(
        CubeWithTime.builder().cubesWithRateAndCurrency(errorSecondCurAndRate).time("2017-05-12")
            .build()
    );

    correctSenderFirst = Sender.builder().name("European Central Bank").build();
    correctSenderSecond = Sender.builder().name("European Central Bank").build();
    errorSenderSecond = Sender.builder().name("American Central Bank").build();

    correctCubeFirst = Cube.builder().cubesWithTime(correctFirstCubeWithTime).build();
    correctCubeSecond = Cube.builder().cubesWithTime(correctSecondCubeWithTime).build();
    errorCubeSecondDiffTimeSameRates = Cube.builder().cubesWithTime(errorSecondDiffTimeSameRates).build();
    errorCubeSecondDiffRatesSameTime = Cube.builder().cubesWithTime(errorSecondDiffRatesSameTime).build();

  }


  @Test
  public void testEqualsSameSenderSubjectCubeurRates() throws Exception {

    Envelope first = Envelope.builder().cube(correctCubeFirst).sender(correctSenderFirst)
        .subject("Reference rates").build();
    Envelope second = Envelope.builder().cube(correctCubeSecond).sender(correctSenderSecond)
        .subject("Reference rates").build();

    assertTrue(first.equals(second));
  }

  @Test
  public void testEqualsDiffSenderSameSubjectCubeCurRates() throws Exception {

    Envelope first = Envelope.builder().cube(correctCubeFirst).sender(correctSenderFirst)
        .subject("Reference rates").build();
    Envelope second = Envelope.builder().cube(correctCubeSecond).sender(errorSenderSecond)
        .subject("Reference rates").build();
    assertFalse(first.equals(second));
  }

  @Test
  public void testEqualsDiffSubjectSameSenderCubeCurRates() throws Exception {

    Envelope first = Envelope.builder().cube(correctCubeFirst).sender(correctSenderFirst)
        .subject("Reference rates").build();
    Envelope second = Envelope.builder().cube(correctCubeSecond).sender(correctSenderSecond)
        .subject("Reference rates ERROR").build();
    assertFalse(first.equals(second));
  }

  @Test
  public void testEqualsDiffCubeTimeSameSenderSubjectCurRates() throws Exception {

    Envelope first = Envelope.builder().cube(correctCubeFirst).sender(correctSenderFirst)
        .subject("Reference rates").build();
    Envelope second = Envelope.builder().cube(errorCubeSecondDiffTimeSameRates).sender(correctSenderSecond)
        .subject("Reference rates").build();
    assertFalse(first.equals(second));
  }

  @Test
  public void testEqualsDiffCubeCurRatesSameSenderSubjectTime() throws Exception {

    Envelope first = Envelope.builder().cube(correctCubeFirst).sender(correctSenderFirst)
        .subject("Reference rates").build();
    Envelope second = Envelope.builder().cube(errorCubeSecondDiffRatesSameTime).sender(correctSenderSecond)
        .subject("Reference rates").build();
    assertFalse(first.equals(second));
  }

}