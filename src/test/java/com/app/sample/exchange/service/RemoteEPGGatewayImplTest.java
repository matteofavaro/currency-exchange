package com.app.sample.exchange.service;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import com.app.sample.exchange.entity.CurrencyDateRate;
import com.app.sample.exchange.service.RemoteEPGGatewayImpl;
import com.app.sample.exchange.service.XmlParserService;
import java.time.LocalDate;
import java.util.List;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

/**
 * Created by matteofavaro on 11/05/2017.
 */
public class RemoteEPGGatewayImplTest {

  private String XML;
  private List<CurrencyDateRate> correctCurrencyDate;

  @Before
  public void setXML(){
    XML =
        "<gesmes:Envelope xmlns:gesmes=\"http://www.gesmes.org/xml/2002-08-01\" xmlns=\"http://www.ecb.int/vocabulary/2002-08-01/eurofxref\">\n"
            + "<gesmes:subject>Reference rates</gesmes:subject>\n"
            + "<gesmes:Sender>\n"
            + "<gesmes:name>European Central Bank</gesmes:name>\n"
            + "</gesmes:Sender>\n"
            + "<Cube>\n"
            + "<Cube time=\"2017-05-12\">\n"
            + "<Cube currency=\"USD\" rate=\"1.0876\" />\n"
            + "<Cube currency=\"JPY\" rate=\"123.82\" />\n"
            + "<Cube currency=\"BGN\" rate=\"1.9558\" />\n"
            + "<Cube currency=\"CHF\" rate=\"1.0963\" />\n"
            + "</Cube>\n"
            + "</Cube>\n"
            + "</gesmes:Envelope>";
  }
@Before
public void setup() {
    LocalDate d = LocalDate.parse("2017-05-12");
  correctCurrencyDate = Lists.newArrayList(
      CurrencyDateRate.builder().date(d).currency("USD").rate(1.0876).build(),
      CurrencyDateRate.builder().date(d).currency("JPY").rate(123.82).build(),
      CurrencyDateRate.builder().date(d).currency("BGN").rate(1.9558).build(),
      CurrencyDateRate.builder().date(d).currency("CHF").rate(1.0963).build()
  );
}

  @Test
  public void testRetriveLastCurrencies() {

    RestTemplate restTemplate = new RestTemplate();
    MockRestServiceServer mockServer = MockRestServiceServer.createServer(restTemplate);

    mockServer.expect(requestTo("localhost")).andExpect(method(HttpMethod.GET))
         .andRespond(withSuccess(XML, MediaType.APPLICATION_XML));
    mockServer.expect(requestTo("localhost")).andExpect(method(HttpMethod.GET))
        .andRespond(withSuccess(XML, MediaType.APPLICATION_XML));
    XmlParserService xps = new XmlParserService(restTemplate);

    ReflectionTestUtils.setField(xps, "remoteApi90Days", "localhost");
    ReflectionTestUtils.setField(xps, "remoteApiDaily", "localhost");
    RemoteEPGGatewayImpl s = new RemoteEPGGatewayImpl(xps);

    List<CurrencyDateRate> toCheck = s.retriveLastestCurrencies();

    mockServer.verify();

    assertTrue(toCheck.equals(correctCurrencyDate));

  }

}