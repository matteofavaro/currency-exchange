package com.app.sample.exchange.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import com.app.sample.exchange.service.XmlParserService;
import com.app.sample.exchange.util.xml.Cube;
import com.app.sample.exchange.util.xml.CubeCurrency;
import com.app.sample.exchange.util.xml.CubeWithTime;
import com.app.sample.exchange.util.xml.Envelope;
import com.app.sample.exchange.util.xml.Sender;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

/**
 * Created by matteofavaro on 11/05/2017.
 */
public class XmlParserServiceTest {


  private String XML;
  private List<CubeCurrency> correctCubeWithRateAndCurrency;
  private List<CubeWithTime> correctCubeWithTime;
  private Cube correctCube;
  private Sender correctSender;

  @Before
  public void setXml() {
    XML =
        "<gesmes:Envelope xmlns:gesmes=\"http://www.gesmes.org/xml/2002-08-01\" xmlns=\"http://www.ecb.int/vocabulary/2002-08-01/eurofxref\">\n"
            + "<gesmes:subject>Reference rates</gesmes:subject>\n"
            + "<gesmes:Sender>\n"
            + "<gesmes:name>European Central Bank</gesmes:name>\n"
            + "</gesmes:Sender>\n"
            + "<Cube>\n"
            + "<Cube time=\"2017-05-12\">\n"
            + "<Cube currency=\"USD\" rate=\"1.0876\" />\n"
            + "<Cube currency=\"JPY\" rate=\"123.82\" />\n"
            + "<Cube currency=\"BGN\" rate=\"1.9558\" />\n"
            + "<Cube currency=\"CHF\" rate=\"1.0963\" />\n"
            + "</Cube>\n"
            + "</Cube>\n"
            + "</gesmes:Envelope>";
  }
  @Before
  public void setup(){
    correctCubeWithRateAndCurrency = Lists.newArrayList(
        CubeCurrency.builder().currency("USD").rate(1.0876).build(),
        CubeCurrency.builder().currency("JPY").rate(123.82).build(),
        CubeCurrency.builder().currency("BGN").rate(1.9558).build(),
        CubeCurrency.builder().currency("CHF").rate(1.0963).build()
    );
    correctCubeWithTime = Lists.newArrayList(
        CubeWithTime.builder().cubesWithRateAndCurrency(correctCubeWithRateAndCurrency)
            .time("2017-05-12").build()
    );
    correctCube = Cube.builder().cubesWithTime(correctCubeWithTime).build();
    correctSender = Sender.builder().name("European Central Bank").build();
  }

  @Test
  public void getDailyECBRates() throws Exception {
    RestTemplate restTemplate = new RestTemplate();
    MockRestServiceServer mockServer = MockRestServiceServer.createServer(restTemplate);

    mockServer.expect(requestTo("localhost")).andExpect(method(HttpMethod.GET))
        .andRespond(withSuccess(XML, MediaType.APPLICATION_XML));

    XmlParserService xps = new XmlParserService(restTemplate);

    ReflectionTestUtils.setField(xps, "remoteApi90Days", "localhost");
    ReflectionTestUtils.setField(xps, "remoteApiDaily", "localhost");

    Optional<CubeWithTime> s = xps.getDailyECBRates();

    assertTrue(s.isPresent());
    assertTrue(s.get().equals(correctCubeWithTime.get(0)));


  }

  @Test
  public void get90DaysECBRates() throws Exception {
    RestTemplate restTemplate = new RestTemplate();
    MockRestServiceServer mockServer = MockRestServiceServer.createServer(restTemplate);

    mockServer.expect(requestTo("localhost")).andExpect(method(HttpMethod.GET))
        .andRespond(withSuccess(XML, MediaType.APPLICATION_XML));

    XmlParserService xps = new XmlParserService(restTemplate);

    ReflectionTestUtils.setField(xps, "remoteApi90Days", "localhost");
    ReflectionTestUtils.setField(xps, "remoteApiDaily", "localhost");

    List<CubeWithTime> s = xps.get90DaysECBRates();
    assertTrue(s.size() == 1);
    assertTrue(s.equals(correctCubeWithTime));

  }

  @Test
  public void testParser() {
    XmlMapper mapper = new XmlMapper();

    try {
      Envelope totest = mapper.readValue(XML, Envelope.class);
      assertNotNull(totest);

      Envelope correct = Envelope.builder().cube(correctCube).sender(correctSender)
          .subject("Reference rates").build();

      assertEquals(correct, totest);

    } catch (IOException e) {
      assertNotNull(e);
    }
  }


}